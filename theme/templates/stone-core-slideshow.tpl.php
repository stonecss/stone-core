<div<?php print $attributes; ?>>
  <div class="stone-core-slideshow__progress"></div>
  <div class="stone-core-slideshow__pager">
    <?php foreach ($slides as $slide): ?>
      <span></span>
    <?php endforeach; ?>
  </div>

  <?php print array_shift($slides); ?>

  <script id="stone-core-slides" type="text/cycle" data-cycle-split="---">
    <?php foreach ($slides as $slide): ?>
      <?php print $slide; ?>
      ---
    <?php endforeach; ?>
  </script>
</div>
