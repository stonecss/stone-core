<div class="stone-core-slideshow__slide">
  <?php print $image; ?>

  <?php if ($title || $description): ?>
  <div class="stone-core-slideshow__caption">
    <div class="stone-core-slideshow__title"><?php print $title; ?></div>
    <div class="stone-core-slideshow__description"><?php print $description; ?></div>
  </div>
  <?php endif; ?>
</div>
