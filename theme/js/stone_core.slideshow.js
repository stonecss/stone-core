(function($) {

Drupal.behaviors.stone_core_slideshow = {
  attach: function (context, settings) {
    var progress = $('.stone-core-slideshow__progress');
    $('.cycle-slideshow').on('cycle-initialized cycle-after', function(e, opts) {
      progress.stop(true).css('width', 0).animate({ width: '100%' }, opts.timeout, 'linear');
    });
  }
};

})(jQuery);
