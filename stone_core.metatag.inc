<?php

/**
 * @file
 * Metatag integration for stone_core.
 */

/**
 * Implements hook_metatag_info().
 */
function stone_core_metatag_info() {
  $info['groups']['refresh'] = array(
    'label' => t('Refresh'),
  );

  $info['tags']['refresh'] = array(
    'label' => t('Refresh'),
    'description' => t('The number of seconds to wait before refreshing the page'),
    'class' => 'DrupalTextMetaTag',
    'group' => 'refresh',
    'element' => array(
      '#theme' => 'metatag_refresh',
    ),
  );

  return $info;
}
