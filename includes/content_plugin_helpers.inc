<?php

function _stone_core_supported_columns() {
  $mqs = array(
    'columns_lg' => array(
      'machine_name' => 'lg',
      'supportive_text' => 'large screens'
    ),
    'columns_md' => array(
      'machine_name' => 'md',
      'supportive_text' => 'medium screens'
    ),
    'columns_sm' => array(
      'machine_name' => 'sm',
      'supportive_text' => 'small screens'
    ),
  );

  return $mqs;
}

function _stone_core_calculate_grid_cell_classes($conf) {
  $classes[] = 'Grid-cell';

  foreach (_stone_core_supported_columns() as $key => $column) {
    if (!empty($conf[$key]) && $conf[$key] > 1) {
      $classes[] = 'u-' . $column['machine_name'] . '-size1of' . $conf[$key];
    }
  }

  return $classes;
}

function _stone_core_default_content_type_edit_form(&$form, &$conf, $cols = FALSE) {
  $form['description'] = array(
    '#title' => t('Description'),
    '#description' => t('Description (administrative)'),
    '#type' => 'textfield',
    '#required' => FALSE,
    '#default_value' => isset($conf['description']) ? $conf['description'] : '',
  );

  $form['title'] = array(
    '#title' => t('Title'),
    '#description' => t('The title of your list'),
    '#type' => 'textfield',
    '#required' => TRUE,
    '#default_value' => isset($conf['title']) ? $conf['title'] : '',
  );

  $form['standard_options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Standard options'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['standard_options']['limit'] = array(
    '#type' => 'textfield',
    '#title' => t('Limit'),
    '#default_value' => isset($conf['limit']) ? $conf['limit'] : 10,
    '#description' => t('Limit the amount of items, 0 for unlimited'),
    '#rules' => array(
      'numeric',
    ),
  );

  $form['standard_options']['paged'] = array(
    '#type' => 'checkbox',
    '#title' => t('Paged'),
    '#default_value' => isset($conf['paged']) ? $conf['paged'] : 1,
    '#description' => t('Should the items be paged?'),
  );

  $form['standard_options']['promoted'] = array(
    '#type' => 'checkbox',
    '#title' => t('Promoted'),
    '#default_value' => isset($conf['promoted']) ? $conf['promoted'] : 0,
    '#description' => t('Only show promoted items'),
  );

  if ($cols) {
    $form['grid_settings'] = array(
      '#type' => 'fieldset',
      '#title' => t('Grid settings'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    );

    $form['grid_settings']['grid'] = array(
      '#type' => 'checkbox',
      '#title' => t('Grid'),
      '#default_value' => isset($conf['grid']) ? $conf['grid'] : 0,
      '#description' => t('Render items in a grid'),
    );

    $form['grid_settings']['grid_options'] = array(
      '#type' => 'fieldset',
      '#title' => t('Grid options'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
      '#states' => array(
        'invisible' => array(
          ':input[name="grid"]' => array('checked' => FALSE),
        ),
      ),
    );

    foreach (_stone_core_supported_columns() as $key => $column) {
      $form['grid_settings']['grid_options'][$key] = array(
        '#title' => t('Columns: ' . strtoupper($column['machine_name'])),
        '#description' => t('Columns to render for ' . $column['supportive_text']),
        '#type' => 'textfield',
        '#required' => FALSE,
        '#default_value' => isset($conf[$key]) ? $conf[$key] : '',
        '#rules' => array(
          'numeric',
        ),
      );
    }
  }
}

function _stone_core_default_content_type_edit_form_submit($form, &$form_state) {
  foreach (array_keys($form_state['plugin']['defaults']) as $key) {
    if (isset($form_state['values'][$key])) {
      $form_state['conf'][$key] = $form_state['values'][$key];
    }
  }

  foreach ($form_state['values'] as $key => $value) {
    $form_state['conf'][$key] = $value;
  }
}

function _stone_core_default_plugin_render($block, $conf, &$options) {
  $block->title = (!empty($conf['title'])) ? check_plain($conf['title']) : '';

  if (!empty($conf['limit'])) {
    $options['limit'] = $conf['limit'];
  }

  $options['paged'] = $conf['paged'];
  $options['promoted'] = $conf['promoted'];
}
