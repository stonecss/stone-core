<?php

/**
 * @file
 * Theming functions for the Core module.
 */

/**
 * Preprocess variables for theme_menu_tree().
 */
function stone_core_preprocess_menu_tree(&$variables) {
  // This runs before Drupal cores implementation of preprocess_menu_tree().
  // We need to preserve the original tree in case it's needed later
  // (template_preprocess_menu_tree() overwrites it).
  $variables['original_tree'] = $variables['tree'];
}

/**
 * Preprocess variables for theme_image_style().
 */
function stone_core_preprocess_image_style(&$variables) {
  if (isset($variables['style_name'])) {
    $variables['attributes']['class'][] = 'image--' . $variables['style_name'];

    if (in_array($variables['style_name'], array(
      'stone_one_half',
      'stone_one_third',
      'stone_two_thirds',
      'stone_one_quarter',
      'stone_three_quarters',
    ))) {
      $class = str_replace('_', '-', str_replace('stone_', 'desk--', $variables['style_name']));
      $variables['attributes']['class'][] = $class;
    }
  }
}

/**
 * Process variables for stone-core-slideshow.tpl.php
 */
function template_preprocess_stone_core_slideshow(&$variables) {
  $variables['attributes_array'] = array(
    'class' => array('stone-core-slideshow', 'cycle-slideshow', 'auto'),
    'data-cycle-fx' => $variables['settings']['effect'],
    'data-cycle-timeout' => $variables['settings']['timeout'],
    'data-cycle-slides' => '.stone-core-slideshow__slide',
    'data-cycle-pager' => '.stone-core-slideshow__pager',
    'data-cycle-pager-template' => '',
    'data-cycle-loader' => true,
    'data-cycle-progressive' => "#stone-core-slides",
    //'data-cycle-pause-on-hover' => 'true',
  );

  $variables['slides'] = array();

  foreach ($variables['items'] as $item) {
    $variables['slides'][] = theme('stone_core_slideshow_slide', array(
      'item' => $item,
      'settings' => $variables['settings'],
    ));
  }

  drupal_add_js(libraries_get_path('cycle2') . '/jquery.cycle2.min.js', array('scope' => 'footer'));
  drupal_add_js(drupal_get_path('module', 'stone_core') . '/theme/js/stone_core.slideshow.js', array('scope' => 'footer'));
}

/**
 * Process variables for stone-core-slide.tpl.php
 */
function template_preprocess_stone_core_slideshow_slide(&$variables) {
  $variables['image'] = theme('image_style', array(
    'path' => $variables['item']['uri'],
    'alt' => $variables['item']['alt'],
    'width' => $variables['item']['width'],
    'height' => $variables['item']['height'],
    'style_name' => $variables['settings']['image_style'],
    'attributes' => array(
      'class' => array('stone-core-slideshow-slide__image'),
    ),
  ));

  $variables['title'] = $variables['item']['title'];
  $variables['description'] = $variables['item']['alt'];
}

/**
 * Theme callback for the refresh meta tag.
 */
function theme_metatag_refresh($variables) {
  $output = '';
  $element = &$variables['element'];

  element_set_attributes($element, array(
    '#name' => 'http-equiv',
    '#value' => 'content',
  ));

  unset($element['#value']);
  return theme('html_tag', $variables);
}

/**
 * Theme a list of content
 */
function theme_content_list($variables) {
  $output = '';

  if (!empty($variables['items'])) {
    $build = node_view_multiple($variables['items']);

    $build['nodes']['#theme_wrappers'] = array('container');

    if (!empty($variables['attributes'])) {
      $build['nodes']['#attributes'] = $variables['attributes'];
    }

    if (!empty($variables['content_attributes'])) {
      foreach ($build['nodes'] as $nid => $node) {
        if (is_numeric($nid)) {
          $build['nodes'][$nid]['#node']->content_attributes = $variables['content_attributes'];
        }
      }
    }

    $build['pager'] = array('#theme' => 'pager');

    if (isset($variables['query']) && isset($variables['query']->element)) {
      $build['pager']['#element'] = $variables['query']->element;
    }

    $output = drupal_render($build);
  }

  return $output;
}
