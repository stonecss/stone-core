<?php
/**
 * @file
 * stone_core.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function stone_core_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "file_entity" && $api == "file_default_displays") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_image_default_styles().
 */
function stone_core_image_default_styles() {
  $styles = array();

  // Exported image style: stone_thumbnail_large.
  $styles['stone_thumbnail_large'] = array(
    'name' => 'stone_thumbnail_large',
    'effects' => array(
      2 => array(
        'label' => 'Scale and crop',
        'help' => 'Scale and crop will maintain the aspect-ratio of the original image, then crop the larger dimension. This is most useful for creating perfectly square thumbnails without stretching the image.',
        'effect callback' => 'image_scale_and_crop_effect',
        'dimensions callback' => 'image_resize_dimensions',
        'form callback' => 'image_resize_form',
        'summary theme' => 'image_resize_summary',
        'module' => 'image',
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 280,
          'height' => 200,
        ),
        'weight' => 1,
      ),
    ),
    'label' => 'Large thumbnail',
  );

  // Exported image style: stone_thumbnail_small.
  $styles['stone_thumbnail_small'] = array(
    'name' => 'stone_thumbnail_small',
    'effects' => array(
      1 => array(
        'label' => 'Scale and crop',
        'help' => 'Scale and crop will maintain the aspect-ratio of the original image, then crop the larger dimension. This is most useful for creating perfectly square thumbnails without stretching the image.',
        'effect callback' => 'image_scale_and_crop_effect',
        'dimensions callback' => 'image_resize_dimensions',
        'form callback' => 'image_resize_form',
        'summary theme' => 'image_resize_summary',
        'module' => 'image',
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 140,
          'height' => 100,
        ),
        'weight' => 1,
      ),
    ),
    'label' => 'Small thumbnail',
  );

  return $styles;
}
